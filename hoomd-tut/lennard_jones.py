import hoomd
import hoomd.md

hoomd.context.initialize("");

hoomd.init.create_lattice(unitcell=hoomd.lattice.sc(a=2.0), n=5)

nl = hoomd.md.nlist.cell();

lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl);

lj.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0);

hoomd.md.integrate.mode_standard(dt=0.005);

all = hoomd.group.all();
hoomd.md.integrate.nvt(group=all, kT=0.5, tau=0.5);

hoomd.analyze.log(filename="log-output.log",
                  quantities=['potential_energy', 'temperature'],
                  period=100,
                  overwrite=True);

hoomd.dump.gsd("trajectory.gsd", period=2e3, group=all, overwrite=True);

hoomd.run(1e5);

import numpy
from matplotlib import pyplot
data=numpy.genfromtxt(fname='log-output.log',skip_header=True)

pyplot.figure(figsize=(4,2.2), dpi=140)
start=30
x=data[:,0]
y=data[:,1]
ave=numpy.mean(y)
pyplot.plot(x[start:],y[start:])
pyplot.xlabel('time step')
pyplot.ylabel('potential_energy')
